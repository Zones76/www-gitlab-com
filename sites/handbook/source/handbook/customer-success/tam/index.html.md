---
layout: handbook-page-toc
title: "Technical Account Management Handbook"
description: "The Technical Account Management team at GitLab is a part of the Customer Success department, acting as trusted advisors to our customers and helping them realize value faster."
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

[<button class="btn btn-primary" type="button" style="padding: 15px 30px; font-size: 24px;">Digital Touch</button>](/handbook/sales/field-operations/customer-success-operations/cs-ops-programs/)
[<button class="btn btn-primary" type="button"  style="padding: 15px 30px; font-size: 24px;">Scale</button>](/handbook/customer-success/tam/segment/scale/)
[<button class="btn btn-primary" type="button"  style="padding: 15px 30px; font-size: 24px;">Named</button>](/handbook/customer-success/tam/segment/named/)
[<button class="btn btn-primary" type="button"  style="padding: 15px 30px; font-size: 24px;">Strategic</button>](/handbook/customer-success/tam/segment/strategic/)

## Mission Statement

Aligning passionate TAMs with customers to ensure their success by...  
- Driving progress aligned with their business outcomes
- Identifying and enabling the customer in their current and future GitLab use cases
- Expanding ROI with GitLab  

## FY23 Goals (Big Rocks)
**Objective:**  Define the 3 ‘big rocks’ to take on in FY23 with the overall goal of moving our team forwards.  These rocks need to move us forwards as a team and as individuals, enabling us to scale, be impactful, and be inspired/fulfilled in our roles. 

**What are 'big rocks'?**  
As outlined in [this article](https://www.forbes.com/sites/hillennevins/2020/01/21/what-are-your-big-rocks/?sh=191f218fae34), they are our priorities, our mission-critical objectives that we need to solve for in the coming year. We arrived at this list through TAM leadership discussions and final input from individual contributors.

### Big Rock 1: (Re)Define the TAM Role

The goal of the TAM is to drive growth. However, the role of the TAM has become increasingly broad over the past few years, deterring from the growth conversation with customers.  As we have added more to the role (migration & infrastructure guidance, cross-functional escalation ownership, consultative support), the ability to focus as a TAM on growth has become increasingly challenging. 

To continue to build a scalable TAM team focused on excellent customer experience and driving customer growth, we need to refine the role of the TAM.  This initiative will include what a TAM is and what a TAM is not.  

Key areas of exploration:
1. Definition by TAM segment: High-touch, Mid-touch, Scale
1. Reducing overlap: Looping in of support/timing
1. Infrastructure conversations and ownership
1. Escalation improvements; streamlining

### Big Rock 2: Expertise in driving CI and DevSecOps adoption & expansion

In FY22 we put considerable focus into use case expansion.  The team have developed a well-rounded understanding of the use cases, but have not been given the space needed to become experts in driving any one use case.  In FY23 we will seek to become experts in driving CI and DevSecOps expansion and maturity, knowing these use cases are the key drivers for customer stickiness and growth. 

Key areas of exploration:
1. Iteration on playbooks
1. Supporting materials
1. Webinars
1. Digital enablement/expansion strategies
1. Reporting & visualization
1. Maturity scoring (customer)

### Big Rock 3: Deepen our customer engagement

The TAM relationship with the customer has been architected primarily on engagement with GitLab admins, who are not responsible for platform adoption or business case value.  Due to the TAM being pigeon-holed into these admin conversations, we are often not engaging with the right people in order to ensure excellent customer experience, use case adoption and growth.  In FY23 we will seek to engage the development team leads and key users of the GitLab platform early and often, to ensure consistent growth strategies with our customers. 

Key areas of exploration: 
1. Expectation setting: from presales
1. Defining engagement strategies for target personas
1. Supporting content creation and digital programs
1. Exec sponsor program
1. User groups/customer forums


## Handbook Directory

[TAM Team Metrics Overview (VIDEO)](https://www.youtube.com/watch?v=9b8VviLG3yE&t=2s)

### TAM Learning & Development

- [Overview of available resources, training plans & career paths](https://about.gitlab.com/handbook/customer-success/tam/tam-development/)

### TAM Responsibilities

- [TAM Onboarding](/handbook/customer-success/tam/tam-onboarding/)
- [TAM Rhythm of Business](/handbook/customer-success/tam/rhythm/)
- [Using Gainsight](/handbook/customer-success/tam/gainsight/)
- [TAM Responsibilities and Services](/handbook/customer-success/tam/services/)
- [TAM and Product Interaction](/handbook/customer-success/tam/product/)
- [TAM and Professional Services Interaction](https://about.gitlab.com/handbook/customer-success/tam/engaging-with-ps/)
- [TAM and Support Interaction](/handbook/customer-success/tam/support/)
- [TAM and Partner Interaction](/handbook/customer-success/tam/engaging-with-partners/)
- [Escalation Process](/handbook/customer-success/tam/escalations/)
  *  [Infrastructure Escalation & Incident Process](https://about.gitlab.com/handbook/customer-success/tam/escalations/infrastructure/)
- [TAM-to-TAM Account Handoff](/handbook/customer-success/tam/account-handoff/)
- [TAM Roleplay Scenarios](/handbook/customer-success/tam/roleplays/)
- [TAM Retrospectives](/handbook/customer-success/tam/retrospectives/)
- [TAM PTO Guidelines](/handbook/customer-success/tam/pto/)
- [TAM READMEs](/handbook/customer-success/tam/readmes/) (Optional)

### Customer Journey

##### TAM-Assigned:

- [Transitioning a Customer from Pre-Sales to Post-Sales](https://about.gitlab.com/handbook/customer-success/pre-sales-post-sales-transition/)
- [Account Engagement and Prioritization](/handbook/customer-success/tam/engagement/)
   - [Non-Engaged Customer Strategies](/handbook/customer-success/tam/engagement/Non-engaged-customer-strategies/)
- [Account Onboarding](/handbook/customer-success/tam/onboarding/)
- [Success Plans](/handbook/customer-success/tam/success-plans/)
   - [Developer & Productivity Metrics](/handbook/customer-success/tam/metrics/)
   - [Sample Questions & Techniques for Getting to Good Customer Metrics](https://about.gitlab.com/handbook/customer-success/tam/success-plans/questions-techniques/)
- [Account Growth Plans](/handbook/customer-success/tam/account-growth/)
- [Stage Enablement & Stage Expansion - The Two Stage Adoption Motions](/handbook/customer-success/tam/stage-enablement-and-expansion/)
   - [Stage Adoption Metrics](/handbook/customer-success/tam/stage-adoption/)
   - [The Customer Value Received with Service Ping](/handbook/customer-success/tam/service-ping-faq/)
   - [Product Usage Data - Definitive Guide to Product Usage Data in Gainsight](/handbook/customer-success/product-usage-data/using-product-usage-data-in-gainsight/)
   - [Customer Use Case Adoption](/handbook/customer-success/product-usage-data/use-case-adoption/)
   - [Metrics Based Product Usage Playbooks](/handbook/customer-success/product-usage-data/metrics-based-playbooks/)
- [Cadence Calls](/handbook/customer-success/tam/cadence-calls/)
- [Executive Business Reviews (EBRs)](/handbook/customer-success/tam/ebr/)
- [Customer Renewal Tracking](/handbook/customer-success/tam/renewals/)
- [Customer Health Assessment and Risk Triage](/handbook/customer-success/tam/health-score-triage/)
- [Risk Types, Discovery & Mitigation](/handbook/customer-success/tam/risk-mitigation/)
- [Workshops and/or Lunch-and-Learns](/handbook/customer-success/tam/workshops/) 


##### Digital Customer Programs:

- [Digital Customer Programs Handbook Page](/handbook/sales/field-operations/customer-success-operations/cs-ops-programs/)
  * [TAM Assigned Customer Programs](/handbook/sales/field-operations/customer-success-operations/cs-ops-programs/#tam-assigned-account-programs)


### TAM Managers

- [TAM Manager Processes](/handbook/customer-success/tam/tam-manager/)
- [TAM Manager QBR Template](https://docs.google.com/presentation/d/1P7Cao5xgILSSrpEGy7Sh09djilnbbIK91IuTs-Xq7mY/edit?usp=sharing) (GitLab Internal)
- [TAM Promotion Template](https://docs.google.com/document/d/1Hg16QVYB2qm0gUGR6H_NPz0cd8OGEuvi3GyDtOsEagY/edit?usp=sharing) (GitLab Internal)


- - -

## What is a Technical Account Manager (TAM)?

GitLab's Technical Account Managers serve as trusted advisors to GitLab customers. They offer guidance, planning, and oversight during the technical deployment and implementation process. They fill a unique space in the overall service lifecycle and customer journey and actively bind together sales, solution architects, customer stakeholders, product management, Professional Services Engineers and support.

See the [Technical Account Manager role description](/job-families/sales/technical-account-manager/) for further information.

### Advocacy

A Technical Account Manager is an advocate for both the customer and GitLab. They act on behalf of customers serving as a feedback channel to development and shaping of the product. In good balance, they also advocate on behalf of GitLab to champion capabilities and features that will improve quality, increase efficiency and realize new value for our customer base.

#### Management

Technical Account Managers maintain the relationships between the customers and GitLab. Making sure that everyone is working towards pre-defined goals and objectives.

#### Growth

Technical Account Managers help to bring GitLab to all aspects of your company, not just software development. They can do this by showing other business units how to use GitLab for their day-to-day tasks and to advocate for new features and functionality that are in demand by other groups.

#### Success

Technical Account Managers make sure that the adoption of GitLab is successful at your company through planning, implementation, adoption, and training.

## Responsibilities and Services

Please reference this page for an overview of the areas your TAM will engage with you in: [TAM Points of Engagement](https://about.gitlab.com/handbook/customer-success/tam/services/)


## TAM Tools

The following articulates where collaboration and customer management is owned:

1. [**Account Management Projects**](https://gitlab.com/gitlab-com/account-management): Shared project between GitLab team members and customer. Used to prioritize/plan work with customer.
   1. [**Ultimate Feature Exploration Checklist**](https://gitlab.com/gitlab-com/account-management/templates/customer-collaboration-project-template/-/blob/master/.gitlab/issue_templates/ultimate_features.md): Issue template that can be made into an issue in a customer's account management project. To be shared with the customer if they want to learn more about GitLab Ultimate features.
1. [**Google Drive**](https://drive.google.com/drive/u/0/folders/0B-ytP5bMib9Ta25aSi13Q25GY1U): Internal. Used to capture call notes and other customer related documents.
1. [**Chorus**](/handbook/business-ops/tech-stack/#chorus): Internal. Used to record Zoom calls.
1. [**Gainsight**](/handbook/customer-success/tam/gainsight/): Internal. Used to track customer health score, logging [customer activity](/handbook/customer-success/tam/gainsight/timeline/#activity-types) (i.e. calls, emails, meetings)

### Education and Enablement

As a Technical Account Manager, it is important to be continuously learning more about our product and related industry topics. The [education and enablement handbook page](/handbook/customer-success/education-enablement) provides a dashboard of aggregated resources that we encourage you to use to get up to speed.

## SFDC useful reports 

### Tracking opportunities for your assigned SALs

To ensure that opportunities are listed with the correct Order Type, [this Salesforce report](https://gitlab.my.salesforce.com/00O4M000004agfP) shows you all of the opportunities that have closed, or are soon to close, with your SALs. Tracking Order Type is important since TAM team quota and compensation depend on this. Please reference the latest [Sales Compensation Plan](https://about.gitlab.com/handbook/finance/sales-comp-plan/) information to know what is counted.

Next steps for you:

1. Customize [this SFDC report](https://gitlab.my.salesforce.com/00O4M000004agfP) where “Account Owner = your SALs”; “TAM = You”
1. Save report
1. Subscribe to report when “Record Count Greater Than 0” and Frequency = Weekly (You’ll get a weekly email as a reminder to look at the report)
1. If you find an opp that is tagged incorrectly, chatter (@Sales-Support) in the opportunity and let them know there is a mistake ([example(https://about.gitlab.com/handbook/customer-success/tam/#tam-tools)])

## Related pages

- [Dogfooding in Customer Success](/handbook/customer-success/#dogfooding)
- [Customer Success & Market Segmentation](/handbook/customer-success/#customer-success--market-segmentation)
- [Responsibility Matrix and Transitions](/handbook/customer-success/#responsibility-matrix-and-transitions)
- [Commercial Sales Customer Success](/handbook/customer-success/comm-sales/)
- [Customer Success' FAQ](/handbook/customer-success/faq/)
- [Using Salesforce within Customer Success](/handbook/customer-success/using-salesforce-within-customer-success/)
- [Customer Success Vision](/handbook/customer-success/vision/)
- [GitLab Positioning](/handbook/positioning-faq/)
- [Product Stages and the POCs for each](/handbook/product/categories/#devops-stages)
- [How to Provide Feedback to Product](/handbook/product/how-to-engage/#feedback-template)
- [Sales handbook](/handbook/sales/)
- [Support handbook](/handbook/support/)
- [Workshops and Lunch-and-Learn slides](https://drive.google.com/drive/folders/1qAymFTiXFEk-lRSNreIhaZ6Z62fdo_y2)
